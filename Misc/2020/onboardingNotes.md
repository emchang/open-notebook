---
title: "Publishing my work notes from Sept/OCt 2020."
---

# :sparkles: Introduction

This post consists of bits and pieces that I have noted down during many :coffee:/:pear: chats with wonderful colleagues of GitLab. There are a lot more that I missed out but I figured I will just share what I have with everyone. Do note that most of the things mentioned are relevant to my role as a support engineer :grin:

## Table of Contents 
- [:sparkles: Introduction](#sparkles-introduction)
  - [Table of Contents](#table-of-contents)
- [:speech_balloon: Wise Words](#speech_balloon-wise-words)
- [:bookmark: Tips](#bookmark-tips)
  - [Onboarding :ship:](#onboarding-ship)
  - [Collaboration 🤝](#collaboration-)
  - [Ticket Queue :ticket:](#ticket-queue-ticket)
  - [Customer Experience :star2:](#customer-experience-star2)
  - [Pairing :dancers:](#pairing-dancers)
  - [Working Remotely :briefcase:](#working-remotely-briefcase)
  - [Learning :books:](#learning-books)
- [:thumbsup: Recommendation](#thumbsup-recommendation)
- [:punch: Challenges Faced](#punch-challenges-faced)
- [:question: Terms](#question-terms)

# :speech_balloon: Wise Words

> It's a marathon, not a sprint.  

> When the problems are harder, it is a sign that the product is getting better. 

# :bookmark: Tips 

## Onboarding :ship:

- Bookmark pages, at least know that it's there so you can search like a pro. No point reading too deep on a topic because the product is always changing. By the time you need it, you will have to go back to the page to get the updated infomation.
- [Omnibus Bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab%20Omnibus.md): highly recommended 👍🏻
  - When spinning up local VM, feel free to be generous on allocating memory. :wink:
- Shadow someone to learn faster 
- Know enough to figure things out, don't be an expert, just answer the questions
- Sometimes it's just about finding the answers before the customer does, because as Support Engineers we have more insights. 
  
## Collaboration 🤝

- Ask questions in public! Always create a slack thread: public conversation so everyone can contribute. However, Slack has 90 days retention. Hence, remember to create an issue to document key points that was discussed via Slack.
- Always cross verify - learn by doing

## Ticket Queue :ticket:

- Try to start from bottom of the queue, look at tickets with SLA 8 hours, then try to figure it out/reproduce the error, just try to reply; even one per day is good enough, can try through pairing
- Find a ticket you can answer - always refer to documentation
- Directed learning vs pairing

## Customer Experience :star2:

- Think about the customer experience, what is the value that you are giving to customer
- Always thank customer for doing something 
- Always say why we need certain info: "Please give us as much information as possible so we can narrow down the problem."
- If the solution does not apply (different versions), always suggest a workaround.


## Pairing :dancers:

- Do regular pairing sessions with team member, for example once a month
- Start working on tickets through pairing
- Doesn't have to be scheduled, ask on team chat if anyone want to do pairing
- Standard practice to take turns and one person replies to one ticket
  
## Working Remotely :briefcase:

- Intentional Communication 
- Look into co-working space - you can do it regularly like once a month etc. Just to feel the office atmosphere if you want to
- Strongly recommend in getting a standing desk 
- Travel and work: cheaper to get monthly apartment

## Learning :books:

- Learn something new everyday, the product is ever changing
- Learn by applying usage on a project, for example deploy Minecraft server using docker, learn through viewing logs and debug

# :thumbsup: Recommendation 
- [Udemy](https://www.udemy.com/) to learn Kubernetes
- Learn Ruby from [Learn Enough](https://www.learnenough.com/ruby-on-rails-6th-edition)
- [Krisp.ai](https://ref.krisp.ai/u/ue9b8f65ab) for noise reduction for zoom calls. (Unfortunately, we can't get [reimbursement for this subscription](https://about.gitlab.com/handbook/finance/expenses/#individual-subscriptions))
- [Alred](https://www.alfredapp.com/) is a productivity application for macOS, which boosts your efficiency with hotkeys, keywords and text expansion.

# :punch: Challenges Faced

- Balance tickets and other things like documentation, code and collaboration
- Depends on how fast you learn and know what to learn, GitLab is HUGE 
- Acknowledge that there is a bug, sometimes there won't be an immediate fix or milestone planned, the best we can do is leave a comment at the issue, ping the product manager about the demand and encourage the customer to participate in the issue. 

# :question: Terms

- Application Support = dotcom   
- Solution Support = Self Managed
- L&R = License and Renewal 
- DRI = Directly Responsible Individual 
- Back up vs. backup:  
The one-word backup works only as an adjective or a noun. When you need a verb, use the two-word phrasal verb back up. For example, when an American football team needs a player to back up their aging quarterback, they might trade for a good backup. [Thanks Grammarist](https://grammarist.com/usage/back-up-backup/)!
