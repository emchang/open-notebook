<!-- 
Title: Weekly Log: [DD-MM-YY]
Remember to bookmark this issue for easier access!
-->

## 💬 Quote of the Week
<!-- Good source: https://www.positivityblog.com/quotes-for-work/ -->

> JUST DO IT.

## 🎯 This Week's Focus
<!-- Try to keep the list to maximum three! -->

<!--**⏰ Reminder: Are you on shift?**-->

## ✅ To Do 
###  Higher Priority

<!-- Recurring tasks:
Start of the month - [ ] Submit Invoice 
End of the month - [ ] Submit Expense
Check last week's "Upcoming next week" section
-->

### Lower Priority
<details><summary>Click to expand</summary>

- [ ] ASAP

**Friday**
- [ ] Check [SWIR](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.a0a90r21a2wh)
- [ ] Fill in wins to GSheet
- [ ] Arrange at least 2 pairings for next week
- [ ] Tick off at least 1 box in Geo training
- [ ] Check out this week's LinkedIn learning

</details>

## 🕐 Time Spent This Week
<!-- Log this at the end of every week to look back. Where did most of my time go? -->

<details><summary>Click to expand</summary>
### Meetings 

</details>

## 💭 Weekly Reflection
<details><summary>Click to expand</summary>

### Wins
<!-- Try to relate to a GitLab value and add to the Wins list at the end of the week, add the relevant labels to the issue, don't forget to add to gform-->

### What I Learn

<!-- Check ~"TIL: Today I Learned 📚" [entries](https://gitlab.com/emchang/open-notebook/-/issues?scope=all&state=all&label_name[]=TIL%3A%20Today%20I%20Learned%20%F0%9F%93%9A) for this week-->
### What did not go well? Why?


</details>

## 🤩 Coming up, Next Week...

<!-- DO NOT EDIT BELOW -->

/label ~"Weekly Log 📝" 
/assign me
/due in 5 days
