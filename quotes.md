# Random Quotes Collected Over The Years


> Mistakes are proof that you are trying.

> "Don't worry about those who talk behind your back, they're behind you for a reason."

> Even bad days have happy moments :)

> Two things define you: Your patience when you have nothing, and your attitude when you have everything.

> Two roads diverged in a wood, and I took the one less traveled by, and that has made all the difference. —ROBERT FROST, “The Road Not Taken”

> How do you spell ‘love'? // Piglet You don't spell it... you feel it. // Pooh

> "The only time you should look down on someone is when you are helping them up."

> "All we have is all we need. All we need is the awareness of how blessed we really are." - Sarah Ban Breathnach

> “Believe nothing, no matter where you read it, or who said it, no matter if I have said it, unless it agrees with your own reason and your own common sense.” -Buddha

> “We make our own fortunes and then call them fate. And what better excuse to choose a path than to insist its our destiny. But at the end of the day, we all have to live with our choices no matter who’s looking over our shoulder.” — Gossip Girl (Gossip Girl, S03E12)

> “When I was 5 years old, my mother always told me that happiness was the key to life. When I went to school, they asked me what I wanted to be when I grew up. I wrote down ‘happy’. They told me I didn’t understand the assignment, and I told them they didn’t understand life.” - John Lennon

> "No matter how hard the past,you can always begin again." -Buddha

> “We are only as strong as we are united, as weak as we are divided.”

> "The problem is not the problem. The problem is your attitude about the problem." - Captain Jack Sparrow

> "Luck is what happens when preparation meets opportunity." - Coach Darrel Royal

> "Choose a job you love, and you will never have to work a day in your life." -Confucius

> "As I am, so are others; as others are, so am I."  Having thus identified self and others, harm no one nor have them harmed. ~ Buddha

> "It is better to conquer yourself than to win a thousand battles." - Buddha

> “The two most important days in your life are the day you are born and the day you find out why."-Mark Twain

> “We make a living by what we get,but we make a life by what we give.” -- Winston Churchill

> "Don't worry about what others think. Most people don't use their brain very often."-Venkat Desireddy

> Worry about your character and not your reputation, because your character is who you are, and your reputation is only what people think of you.

> "The richest man is not he who has the most, but he who needs the least." - Unknown Author 

> "What you must become is what you would do for the rest of your life for free."-Jeckov kanani

> "The meaning of life is to give life meaning."-Hudgins, Ken

> "I am better than i was yesterday,but not as good as i will be tomorrow."

> "Just because life has dealt us one hard blow, there is no reason for us to be unhappy for ever." - Anne Fine

> ' WE ARE WHAT WE THINK. ALL THAT WE ARE ARISES WITH OUR THOUGHTS, WE MAKE THE WORLD. ' -LORD BUDDHA.

> Life is a journey, take it slow. Enjoy every second of it, before you go. -Joe Brooks 

> Life has 3 C's: Choice, Chance, Change. You have to make the Choice, to take the Chance, if you want anything to Change.

> "Quality over quantity. Imagination over imitation. Integrity over image. Legacy over latest. Always. " - Philip Wang

> "The best things in the world are not things."

> “Peeing is like a good book in that it is very, very hard to stop once you start.” ― John Green, Paper Towns

> "A hero can be anyone. Even if they're just putting a coat over the shoulders of a young boy to let him know this isn't the end" - The Dark Knight Rises

> “And I’ve realized that the Beatles got it wrong. Love isn’t all we need—love is all there is.” ― Morgan Matson, Second Chance Summer 

> To enjoy the rainbow, first enjoy the rain - Paulo Coelho

> "Victory is always possible for the person who refuses to stop fighting." -Napoleon Hil

> The difference between school and life? In school, you're taught a lesson and then given a test. In life, you're given a test that teaches you a lesson. - Tom Bodett

> There are three phases to awareness: to look, to see, and to perceive. A camera looks. A mind sees. A heart perceives - Unknown

> "The secret of success is constancy of purpose." - Benjamin Disraeli

> “Happiness is an attitude. We either make ourselves miserable, or happy and strong. The amount of work is the same.” - Francesca Reigler

> "The only way around is through." - Robert Frost

> “Great dancers are not great because of their technique, they are great because of their passion.” - Martha Graham

> "We don't stop playing because we grow old, we grow old because we stop playing."

> "I cry because people are stupid and that makes me sad." - Dr. Sheldon Coope

> "Suffering produces character, and character produces hope, and hope does not disappoint us." - Jeremy Lin 

> "Take care of your body. It's the only place you have to live." - Jim Rohn

> "I'd rather be hated for who I am, than loved for who I am not" - Kurt Cobain

> The only disability in life is a bad attitude. – Scott Hamilton

> “Earth provides enough to satisfy every man's needs, but not every man's greed.” ― Mahatma Gandhi

> "There is no such thing as failure. There are only results." - Tony Robbins

> "We have two options, medically & emotionally: give up, or Fight Like Hell." - Lance Armstrong

> Learn from yesterday, live for today, & hope for tomorrow.

> “A decision is made with the brain. A commitment is made with the heart. Therefore, a commitment is much deeper and more binding than a decision.” — Nido Qubein

> “Be who you are and say what you feel because those who mind don't matter and those who matter don't mind.” -Dr. Seuss

> "There is a real magic in enthusiasm. It spells the difference between mediocrity &accomplishment." -Norman Peale

> “Sometimes the hardest thing in life is to know which bridge to cross and which to burn.” -The International (2009)

> "Faith is taking the first step...even when you don't see the whole staircase." - Martin Luther King, Jr.

> “Twenty years from now you will be more disappointed by the things that you didn't do than by the ones you did do. So throw off the bowlines. Sail away from the safe harbor. Catch the trade winds in your sails. Explore. Dream. Discover.” ― Mark Twain

> "Strength does not come from physical capacity. It comes from an indomitable will." -Mahatma Gandhi

> “All we have to decide is what to do with the time that is given us.” — J.R.R. Tolkien - The Fellowship of the Ring

> “I'm selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can't handle me at my worst, then you sure as hell don't deserve me at my best.” ― Marilyn Monroe

> “You've gotta dance like there's nobody watching, Love like you'll never be hurt, Sing like there's nobody listening, And live like it's heaven on earth.” ― William W. Purkey

> “Insanity is doing the same thing, over and over again, but expecting different results.” ― Albert Einstein

> “Don't walk behind me; I may not lead. Don't walk in front of me; I may not follow. Just walk beside me and be my friend.” ― Albert Camus

> “You only live once, but if you do it right, once is enough.” ― Mae West

> “Darkness cannot drive out darkness; only light can do that. Hate cannot drive out hate; only love can do that.” ― Martin Luther King Jr.

> “Here's to the crazy ones. The misfits. The rebels. The troublemakers. The round pegs in the square holes. The ones who see things differently. They're not fond of rules. And they have no respect for the status quo. You can quote them, disagree with them, glorify or vilify them. About the only thing you can't do is ignore them. Because they change things. They push the human race forward. And while some may see them as the crazy ones, we see genius. Because the people who are crazy enough to think they can change the world, are the ones who do.”  ― Apple Inc.

> “Always forgive your enemies; nothing annoys them so much.” ― Oscar Wilde

> “Live as if you were to die tomorrow. Learn as if you were to live forever.” ― Mahatma Gandhi

> “In three words I can sum up everything I've learned about life: it goes on.” ― Robert Frost

> “If you tell the truth, you don't have to remember anything.” ― Mark Twain

> “Friendship is born at that moment when one person says to another: "What! You too? I thought I was the only one.” ― C.S. Lewis

> “Be yourself; everyone else is already taken.” ― Oscar Wilde

> “Be the change that you wish to see in the world.” ― Mahatma Gandhi

> “Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.” ― Albert Einstein

> “Don't cry because it's over, smile because it happened.” -  Dr. Seuss

> “If you love two people at the same time, choose the second one. Because if you truly loved the first one, you wouldn’t love another. There are four questions of value in life… What is sacred? Of what is the spirit made? What is worth living for, and what is worth dying for? The answer to each is the same. Only love.” - Johnny Depp

> "Study as if you are motivated for the outcome of your hard work."

> "Having a role model in life is a great thing to have; one who provides us with direction and inspiration. However, we will forever be restricted by that person's limitations if we live within their boundaries. Be influenced, but set your own standards and develop your own principals, if you are ever to live beyond someone else's dreams."

> "Happiness is not something you postpone for the future; it is something you design for the present." - Jim Rohn

> "Believe you can and you're halfway there." - Theodore Roosevelt

> "The first and best victory is to conquer self." -Plato

> "Monsters are real, so are ghosts. They live inside of us, and they even take control of us sometimes." - Stephen King

> "Love is funny. Makes you happy, sad and makes you do all sorts of things you never thought you'd do before" -ILY Phillip Morris (2009) Movie quote

> "When you see the present, you see the future. By understanding the present you understand the past." - Ajahn Chah

> "The best and most beautiful things in the world cannot be seen or even touched. They must be felt with the heart. " - Helen Keller 

> "there are only two ways to live; one is live as though nothing is a miracle; the other is live as though everything is a miracle." - Albert Einstein

> "When life gives you a hundred reasons to cry, show life that you have a thousand reasons to smile." - Unknown

> “Life is not measured by the number of breaths we take, but by the moments that take our breath away.” ― Maya Angelou

> "When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one that has opened for us." - Alexander Graham Bell

> "Again, you can't connect the dots looking forward; you can only connect them looking backwards. So you have to trust that the dots will somehow connect in your future. You have to trust in something - your gut, destiny, life, karma, whatever. This approach has never let me down, and it has made all the difference in my life. " - Steve Jobs 

> "Time you enjoy wasting, was not wasted." - John Lennon

> "Do not dwell in the past, do not dream of the future, concentrate the mind on the present moment." -Buddha

>  "A creative man is motivated by the desire to achieve, not by the desire to beat others." - Ayn Rand

> “The only reason we don’t have what we want in life is the reasons we create why we can’t have them.” ~Tony Robbins

> "A person who never made a mistake never tried anything new." - Albert Einstein

> "If we wait for the moment when absolutely everything is ready, we shall never begin." - Ivan Turgenev

>  “The real heroes anyway aren’t the people doing things; the real heroes are the people noticing things, paying attention.” — John Green, The Fault in Our Stars

> "Take calculated risks. That is quite different from being rash." - George S. Patton

> “I think the saddest people always try their hardest to make people happy because they know what it's like to feel absolutely worthless and they don't want anyone else to feel like that.” - Robin Williams

> ‘We Do Not Remember Days, We Remember Moments…’- Italian Poet Cesare Pavese.

> You don't always get what you want. But if you get what you want, that's the one.

> Just remember, I would rather listen to you than attending your funeral.

> All I know is whatever I do, I'll never let a worthy friend behind.

> When a bad day comes, always remember there is tomorrow.

> Life is like a piano: white keys are happy moments and black keys are sad moments. But remember both keys are played together to give sweet music.
