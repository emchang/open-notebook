# emchang's Open Notebook 📖

### 🎯 Objective 

This was something I've been meaning to do but I have yet to put things in action. So here is my 1st attempt to make my ideas to reality.

This project will be used to track my work and progress. I will be using it actively for my daily work and long term career goal. 

### 🖊️ How Will I Use This Project 

I will use repo to write my notes. This is still work in progress, I mainly use Issues more. 

I will use GitLab issues mainly to organise my tasks and ideas and track my progress. Then, sort the issues with labels.

Here is a list of labels I use:

GitLab Values:

- [Collaboration 🤝](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Collaboration+%F0%9F%A4%9D)
- [Results📈](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Results%F0%9F%93%88)
- [Efficiency⏱️](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Efficiency%E2%8F%B1%EF%B8%8F)
- [DIB🌐](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=DIB%F0%9F%8C%90)
- [Iteration👣](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Iteration%F0%9F%91%A3)
- [Transparency👁️](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Transparency%F0%9F%91%81%EF%B8%8F)


Categories:

- [Idea💡](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Idea+%F0%9F%92%A1)
- [Investigation 🕵️](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Investigation+%F0%9F%95%B5%EF%B8%8F)
- [Reflection 🪞](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Reflection+%F0%9F%AA%9E)
- [TIL: Today I Learned 📚](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=TIL%3A+Today+I+Learned+%F0%9F%93%9A)
- [Weekly Log 📝](https://gitlab.com/emchang/open-notebook/-/issues?label_name%5B%5D=Weekly+Log+%F0%9F%93%9D)

### 💭 Things to Keep in Mind 

This project is **PUBLIC**, to align with GitLab's transparency value. I should not mention any sensitive data. 

# Emily Chang's README 📝

Kindly refer to this page for my [README](https://about.gitlab.com/handbook/engineering/readmes/emily-chang/)

